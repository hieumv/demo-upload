<?php

use App\Http\Controllers\BlogController;
use App\Http\Controllers\PlayersController;
use App\Http\Controllers\MailController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/test-mail',[MailController::class,'testMail']);
Route::prefix('blog')->name('blog.')->group(function () {
    Route::get('/', [BlogController::class,'index'])->name('index');
    // them cau thu
    Route::get('/add', [BlogController::class,'add'])->name('add');
    Route::post('/add', [BlogController::class,'postAdd']);
    // sua cau thu
    Route::get('/edit/{id}', [BlogController::class,'edit'])->name('edit');
    Route::post('/edit/{id}', [BlogController::class,'postEdit'])->name('post-edit');

    Route::get('/delete/{id}',[BlogController::class,'delete'])->name('delete');
    // demo validate request
    Route::get('/demo',[BlogController::class,'demo'])->name('demo');
    Route::post('/demo',[BlogController::class,'postDemo'])->name('post-demo');
    // tao route de gui mail

});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// tao route moi
Route::prefix('players')->name('players.')->group(function(){
    Route::get('/',[PlayersController::class,'index'])->name('index');
    Route::get('/add',[PlayersController::class,'add'])->name('add');
    Route::post('/add',[PlayersController::class,'postAdd'])->name('post-add');
});
