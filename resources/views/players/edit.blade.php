@extends('layout')
@section('title')
    {{$title}}
@endsection
@section('noidung')
    <h1>{{$title}}</h1>
    <form action="" method="POST">
        <div>
            <label for="">Name</label>
            <input type="text" value="{{old('name') ?? $dataDetail[0]->name}}" name="name" class="form-control" value="1" placeholder="name">
            @error('name')
                <span style="color:red;">{{$message}}</span>
            @enderror
        </div>
        <div>
            <label for="">Age</label>
            <input type="text" value="{{old('age') ?? $dataDetail[0]->age}}" name="age" class="form-control" >
            @error('age')
                <span style="color:red;">{{$message}}</span>
            @enderror

        </div>
        <div>
            <label for="">National</label>
            <input type="text" value="{{old('national') ?? $dataDetail[0]->national}}" name="national" class="form-control" >
            @error('national')
                <span style="color:red;">{{$message}}</span>
            @enderror
        </div>
        <div>
            <label for="">Position</label>
            <input type="text" value="{{old('position') ?? $dataDetail[0]->position}}" name="position" class="form-control" >
            @error('position')
                <span style="color:red;">{{$message}}</span>
            @enderror
        </div>
        <div>
            <label for="">Salary</label>
            <input type="text" value="{{old('salary') ?? $dataDetail[0]->salary}}" name="salary" class="form-control" >
            @error('salary')
                <span style="color:red;">{{$message}}</span>
            @enderror
        </div>
        <br>
        <button type="submit" class="btn btn-success">Cap nhat</button>
        <a href="{{route('blog.index')}}" type="button" class="btn btn-primary">Quay ve</a>   
        @csrf
-    </form>
    <br>
    
@endsection