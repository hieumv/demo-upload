@extends('layout')
@section('title')
    {{$title}}
@endsection
@section('noidung')
    <h1>{{$title}}</h1>
    <table class="table table-bodered">
        <thead class="bg-light">
           <tr>
            <th>name</th>
            <th>age</th>
            <th>national</th>
            <th>posittion</th>
            <th>salary</th>
            <td>Edit</td>
            <td>Delete</td>
           </tr>
        </thead>
        <tbody>
            @if (!empty($players))
                @foreach ($players as $player)
                <tr>
                    <td>{{$player->name}}</td>
                    <td>{{$player->age}}</td>
                    <td>{{$player->national}}</td>
                    <td>{{$player->position}}</td>
                    <td>{{$player->salary}}</td>
                    <td><a href="{{route('blog.edit',['id'=>$player->id])}}" class="btn btn-warning">Edit</a></td>                    
                    <td><a href="{{route('blog.delete',['id'=>$player->id])}}" class="btn btn-info">Delete</a></td>                    

                </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="5">Khong co cau thu</td>
                </tr>
            @endif
        </tbody>
    </table>
    <a href="{{route('players.add')}}" type="button" class="btn btn-primary">Them cau thu</a>   
@endsection