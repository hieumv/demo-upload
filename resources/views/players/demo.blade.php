@extends('layout')
@section('title')
    {{$title}}
@endsection
@section('noidung')
    <h1>{{$title}}</h1>
    @if ($errors->any())
        <div class="alert alert-danger">
            <p>Them cau thu khong thanh cong</p>
        </div>
    @endif
    <form action="" method="POST">
        <div>
            <label for="">Name</label>
            <input type="text" value="{{old('name')}}" name="name" class="form-control" >
            @error('name')
                <span style="color:red;">{{$message}}</span>
            @enderror
        </div>
        <div>
            <label for="">Age</label>
            <input type="text" value="{{old('age')}}" name="age" class="form-control" >
            @error('age')
                <span style="color:red;">{{$message}}</span>
            @enderror

        </div>
        <div>
            <label for="">National</label>
            <input type="text" value="{{old('national')}}" name="national" class="form-control" >
            @error('national')
                <span style="color:red;">{{$message}}</span>
            @enderror
        </div>
        <div>
            <label for="">Position</label>
            <input type="text" value="{{old('position')}}" name="position" class="form-control" >
            @error('position')
                <span style="color:red;">{{$message}}</span>
            @enderror
        </div>
        <div>
            <label for="">Salary</label>
            <input type="text" value="{{old('salary')}}" name="salary" class="form-control" >
            @error('salary')
                <span style="color:red;">{{$message}}</span>
            @enderror
        </div>
        <br>
        <button type="submit" class="btn btn-success">Them moi</button>
        <a href="{{route('blog.index')}}" type="button" class="btn btn-primary">Quay ve</a>   
        @csrf
-    </form>
    <br>
    
@endsection