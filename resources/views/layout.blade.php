<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js" integrity="sha512-STof4xm1wgkfm7heWqFJVn58Hm3EtS31XFaagaa8VMReCXAkQnJZ+jEy8PCC/iT18dFy95WcExNHFTqLyp72eQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    @yield('css')
</head>
<body>
   <div class="wrapper">
            <header class="py-3 shadow ">
                <div class="container d-flex justify-content-center">
                    <h1>Quan ly cau thu</h1>
                </div>
            </header>
       
            <main class="py-5">
                <div class="container">
                    <div class="row">
                        <div class="col-8 offset-2">
                            @yield('noidung')
                            
                        </div>
                    </div>
               
                </div>
        </main>
       
       
       <footer>
            <div class="container">
                copy right Hieu mai
            </div>
       </footer>
   </div>
   <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
   {{-- <script src="{{asset('assets/js/jquery.js')}}"></script> --}}
   <script src="{{asset('assets/js/custom.js')}}"></script>
   <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script> CKEDITOR.replace('demo', {
        filebrowserBrowseUrl: {{ asset('ckfinder/ckfinder.html') }},
        filebrowserImageBrowseUrl: {{ asset('ckfinder/ckfinder.html?type=Images') }},
        filebrowserFlashBrowseUrl: {{ asset('ckfinder/ckfinder.html?type=Flash') }}',
        filebrowserUploadUrl: {{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }},
        filebrowserImageUploadUrl: {{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }},
        filebrowserFlashUploadUrl: {{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}
    }
); </script>

   @yield('javascript')
</body>
</html>