@extends('layout')
@section('title')
    {{$title}}
@endsection
@section('noidung')
    <h1>{{$title}}</h1>
    @if ($errors->any())
        <div class="alert alert-danger">
            <p>Them cau thu khong thanh cong</p>
        </div>
    @endif
    <form action="" method="POST" enctype="multipart/form-data">
        <div>
            <label for="">Name</label>
            <input type="text"  name="name" class="form-control" value="{{old('name')}}">
            @error('name')
                <span style="color:red;">{{$message}}</span>
            @enderror
        </div>
        <div>
            <label for="">Avatar</label>
            <input type="file"  name="avatar[]" class="form-control" multiple>
            @error('avatar')
                <span style="color: red;">{{$message}}</span>
            @enderror
            
        </div>
        <div>
            <label for="">Age</label>
            <input type="text"  name="age" class="form-control" value="{{old('age')}}">
            @error('age')
                <span style="color:red;">{{$message}}</span>
            @enderror

        </div>
        <div>
            <label for="">National</label>
            <input type="text"  name="national" class="form-control" value="{{old('national')}}">
            @error('national')
                <span style="color:red;">{{$message}}</span>
            @enderror
        </div>
        <div>
            <label for="">Position</label>
            <input type="text"  name="position" class="form-control" value="{{old('position')}}">
            @error('position')
                <span style="color:red;">{{$message}}</span>
            @enderror
        </div>
        <div>
            <label for="">Salary</label>
            <input type="text"  name="salary" class="form-control" value="{{old('salary')}}">
            @error('salary')
                <span style="color:red;">{{$message}}</span>
            @enderror
        </div>
        <div>
            <label for="">Ckeditor</label>
            <textarea name="" id="demo" cols="115" rows="5">

            </textarea>
        </div>
        <br>
        <button type="submit" class="btn btn-success">Them moi</button>
        <a href="{{route('blog.index')}}" type="button" class="btn btn-primary">Quay ve</a>   
        @csrf
-    </form>
    <br>
    
@endsection