@extends('layout')
@section('title')
    {{$title}}
@endsection
@section('noidung')
    <h1>{{$title}}</h1>
    <table class="table table-bodered">
        <thead class="bg-light">
           <tr>
            <th>name</th>
            <td width="100px">avatar</td>
            <th>age</th>
            <th>national</th>
            <th>posittion</th>
            <th>salary</th>
            <td>Edit</td>
            <td>Delete</td>
           </tr>
        </thead>
        <tbody>
            @if (!empty($listUsers))
                @foreach ($listUsers as $user)
                <tr>
                    <td>{{$user->name}}</td>
                    <td><img src="upload/{{$user->avatar}}" alt="" width="100px"></td>
                    <td>{{$user->age}}</td>
                    <td>{{$user->national}}</td>
                    <td>{{$user->position}}</td>
                    <td>{{$user->salary}}</td>
                    <td><a href="{{route('blog.edit',['id'=>$user->id])}}" class="btn btn-warning">Edit</a></td>                    
                    <td><a href="{{route('blog.delete',['id'=>$user->id])}}" class="btn btn-info">Delete</a></td>                    

                </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="5">Khong co cau thu</td>
                </tr>
            @endif
        </tbody>
    </table>
    <a href="{{route('blog.add')}}" type="button" class="btn btn-primary">Them cau thu</a>   
    <div class="btn btn-alert check">check</div>
    <script>
        $(document).ready(function () {
            $('.check').click(function(){
                alert("xin chao");
            })
        });
    </script>
@endsection