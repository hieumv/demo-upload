<?php

namespace App\Http\Controllers;
use App\Http\Requests\PlayersRequest;

use Illuminate\Http\Request;

use DB;
use Image;

use App\Models\Players;

class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
        $title = "Danh sach cau thu";
        // $listUsers = DB::select('SELECT * FROM players');
        $listUsers = Players::all();
       
        return view('blog.lists',compact('title','listUsers'));
    }

    public function add(){
        $title = "Them cau thu";
        return view('blog.add',compact('title'));
    }

    public function demo(){
        $title = "Validate";
        return view('blog.demo',compact('title'));
    }

    public function postDemo(PlayersRequest $request){
        
    }

    // xu ly add cau thu
    public function postAdd(PlayersRequest $request){
        if($request->hasfile('avatar'))
        {

            foreach($request->file('avatar') as $image)
            {

                $name = $image->getClientOriginalName();
                $name_2 = "2".$image->getClientOriginalName();
                $name_3 = "3".$image->getClientOriginalName();

                $image->move('upload/product/', $name);
                
                $path = public_path('upload/product/' . $name);
                $path2 = public_path('upload/product/' . $name_2);
                $path3 = public_path('upload/product/' . $name_3);

                Image::make($image->getRealPath())->save($path);
                Image::make($image->getRealPath())->resize(50, 70)->save($path2);
                Image::make($image->getRealPath())->resize(200, 300)->save($path3);
                
                $data[] = $name;
            }
        }

        dd($data);

        $file = $request->avatar;
        $dataInsert['avatar'] = $file->getClientOriginalName();

        
        // DB::insert('INSERT INTO `players` (name,age,national,position,salary) values (?, ?,?,?,?)', $dataInsert);
        if(Players::create($dataInsert)){
            $file->move('upload/',$file->getClientOriginalName());
            return redirect()->route('blog.index');
        }else{
            // 
        }
        

    }

    public function edit($id){
        $title = "cap nhat thong tin";
        $dataDetail = DB::select('SELECT * FROM players where id = ?', [$id]);
        
        return view('blog.edit',compact('title','dataDetail'));
    }

    public function postEdit( PlayersRequest $request, $id=0){
        $player = Players::findOrFail($id);
        $dataUpdate = $request->all();
        if($player->update($dataUpdate)){
            return redirect()->route('blog.index');

        }

        return redirect()->route('blog.index');
    }

    public function delete($id){
        // DB::delete('DELETE FROM players WHERE id = ?', [$id]);
        Players::where('id',$id)->delete();
        return redirect()->route('blog.index');
    }
}
